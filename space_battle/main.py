# encoding utf-8
import pygame
import os
import sys

sys.path.append(os.path.join(sys.path[0], '..'))

from defaults import WINDOW_HEIGHT, WINDOW_WIDTH
from entities.player import Player

if __name__ == '__main__':
    pygame.init()
    window = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Game')
    clock = pygame.time.Clock()
    flag_game = True

    holder = []
    player = Player(
        pygame.image.load('resources//images/jet1.png'),
    )

    while flag_game:
        # Количество обновлений логики в секнду (ФПС)
        clock.tick(60)
        # Блок обработки событий
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                flag_game = False
        if not flag_game:
            break
        # Блок управления персонажем
        keys = pygame.key.get_pressed()
        if keys[pygame.K_UP]:
            player.y -= player.velocity
        if keys[pygame.K_DOWN]:
            player.y += player.velocity
        if keys[pygame.K_LEFT]:
            player.x -= player.velocity
        if keys[pygame.K_RIGHT]:
            player.x += player.velocity
        # Блок отрисовки
        window.fill((51, 51, 51))
        player.draw(window)
        pygame.display.update()

    pygame.quit()

