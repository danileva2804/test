import pygame
from space_battle.entities.world_object import WorldObject


class Player(WorldObject):
	def __init__(self, image, *args, **kwargs):
		super(Player, self).__init__(*args, **kwargs)
		self.window = pygame.display.get_surface()
		self.img = image

	def draw(self, window):
		window.blit(self.img, (self.x, self.y))

	def shoot(self, target, holder):
		pass

	def weapon(self):
		pass
